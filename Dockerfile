FROM openjdk:8-jre-slim

RUN apt update -y

RUN useradd -ms /bin/bash demo

USER demo

WORKDIR /app

ADD docker-demo.jar /app

EXPOSE 8080

CMD java -jar docker-demo.jar
